<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />

Philippe Gaultier
=================


> I am a Software Engineer and DevOps  focusing on delivering *simple*, high quality software that one can easily deploy, troubleshoot and maintain. 

----------------    ---------------------------------
Piesenkofen 8A                     <philigaultier@gmail.com>
84546 Egglkofen            [https://github.com/gaultier](https://github.com/gaultier)
Germany                                    +49 152 04806376
----------------    ---------------------------------

Experience
----------

**Full-stack Software Engineer and DevOps** *PPRO, Munich, Germany; 2017-present*

I write and deploy end-to-end Fintech web services that help making millions of transactions for billions of euros, in a variety of tech stacks (*Go*, *C++*, *Kotlin*, *Docker*, *Kubernetes*, *Terraform*). I also helped moving multiple services from the data center to the cloud without disruption.
In the later years, I are moving more and more to DevOps & SRE responsabilities such as adding logging, metrics, alerting, distributed tracing and performance monitoring to existing software in order to increase their reliability, as well as guaranteeing SLAs crucial to the business.

**Full-stack Software Engineer** *EdgeLab, Lausanne, Switzerland; 2015-2017*

I joined a Fintech startup and helped the product (risk analytics & investment decision making application) go to the next level by introducing internationalization, OAuth, Continuous Integration, and upgrading to the latest language standards (*ECMAScript 6*, *C++14*).

**CRNS Intern Software Engineer experimenting with Oculus Rift (VR)** *CNRS, Strasbourg, France; 2014*

3D Oculus Rift (virtual reality) visualization software of astronomical data for children & researchers in *C*, *C++* and *OpenGL*.

**Intern Software Engineer** *Crédit Mutuel (Bank), Strasbourg, France; 2013*

I migrated a *COBOL* mainframe accounting application to a *C#* web application.

 
Technical experience
--------------------

Libprom
:   [A pure C library](https://github.com/gaultier/libprom) to parse Prometheus (monitoring software) metrics in a text format, with no dependencies. This allows any software with C interoperability to consume the metrics produced by applications in the Prometheus format. It comes with a Prometheus format to YAML CLI converter as an example.

Programming Languages
:   **Go** Professional experience including the latest standards

:   **C:** Professional experience including the latest standards

:   **Rust**: Enthusiast with several open-source projects to my belt

:   Solid knowledge of **x64 assembly**, **Awk**, **Shell**, **Terraform**

Education
---------

2012-2015
:   **Masters in Computer Science & Engineering Degree** *ENSIIE, Strasbourg, France*

2010-2012
:   **Higher School Preparatory Classes** *Lycée Kléber, Strasbourg, France*

2007-2012
:   **Abibac (dual French & German Highschool Diploma)** *Belfort, France*

Hobbies
-------
* Languages: French (native), English (proficient), German (proficient), Bavarian (adept)
* Weightlifting
* Philology
